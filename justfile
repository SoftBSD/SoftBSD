#!/usr/bin/env -S just --justfile
# NOTE: use canonical `just --fmt` formatting

mode := 'debug'
machine := 'amd64'
arch := 'amd64'
sudo := 'doas'
jobs := `sysctl -n hw.ncpu`
vers := `date +%Y-%m-%d` + "-" + mode
img := 'org.softbsd.System:' + arch + ':' + vers
diskpool := 'softbsd-' + arch + '-' + vers
buildtop := '_build'
tmpdir := buildtop / 'dest'
objdir := buildtop / 'obj'
fetchdir := buildtop / 'pkg'
rustdir := buildtop / 'rust'
diskmount := '/tmp/softbsd-wip'
export MAKEOBJDIRPREFIX := absolute_path(objdir)
export CARGO_TARGET_DIR := absolute_path(rustdir)
export CROSS_TOOLCHAIN := absolute_path('llvmroot.mk')
export LLVM_ROOT := '/usr/local/llvm16'
export CC := LLVM_ROOT / 'bin/clang'
export CXX := LLVM_ROOT / 'bin/clang++'
export CPP := LLVM_ROOT / 'bin/clang-cpp'
export LD := LLVM_ROOT / 'bin/ld.lld'
export TARGET := machine
export TARGET_ARCH := arch
export KERNCONF := 'GENERIC'
export SRCCONF := '/dev/null'
export SRC_ENV_CONF := '/dev/null'
export WITH_META_MODE := '1'
export WITHOUT_SSP := '1'
export NO_ROOT := '1'
export NO_SILENT := '0'

[private]
@default:
    echo "SoftBSD Build System powered by just!"
    just --list

# Run $EDITOR with the build system's environment, importantly CARGO_TARGET_DIR for the Rust language server launched by the editor
[no-cd]
edit *args:
    $EDITOR {{ args }}

alias e := edit

# Run make inside of a particular subdirectory of base with the build system's environment
compilepart dir *args:
    make -j{{ jobs }} "-C{{ 'base' / dir }}" {{ args }}

# Build a FreeBSD base component
compilebase part:
    make -j{{ jobs }} -Cbase build{{ part }}

# Install a FreeBSD base component into the running system (FOR DOING REALLY BAD THINGS)
[private]
installbase part:
    unset NO_ROOT; make -j{{jobs}} -Cbase install{{part}}

# Build a Rust component
compilerust dir:
    cd "{{ dir }}" && cargo build --profile "{{ if mode == 'debug' { 'dev' } else { mode } }}"

# Build all known components
compile: (compilerust '.') (compilebase 'world') (compilebase 'kernel') (compilebase 'etc')

pkgenv := 'INSTALL_AS_USER=yes IGNORE_OSVERSION=yes'
pkgflg := '-C /dev/null -R pkgrepos -o PKG_DBDIR="' + fetchdir + '" -o PKG_CACHEDIR="' + fetchdir + '" -o SYSLOG=no'
# Fetch prebuilt firmware packages from the FreeBSD pkg repo
fetch:
    mkdir -p "{{ fetchdir }}"
    {{ pkgenv }} pkg {{ pkgflg }} update -r FreeBSD_{{ machine }}
    {{ pkgenv }} pkg {{ pkgflg }} fetch -r FreeBSD_{{ machine }} -Uyo "{{ fetchdir }}" -d \
        gpu-firmware-kmod iwmbt-firmware wifi-firmware-mt76-kmod wifi-firmware-rtw88-kmod wifi-firmware-rtw89-kmod
# TODO: cpu ucode, following https://reviews.freebsd.org/D41406

# Run a pkg command in the fetch configuration
pkg *args:
    {{ pkgenv }} pkg {{ pkgflg }} {{ args }}

# Build and install the OS into a destdir
install destdir: compile fetch
    make -j{{ jobs }} -Cbase installworld installkernel installetc \
        "DESTDIR={{ absolute_path(destdir) }}"
    # install -C thingy "{{ destdir / 'usr/sbin' }}"
    install -C "{{ rustdir / mode }}/soft" "{{ destdir / 'usr/sbin' }}"
    install -C "{{ rustdir / mode }}/serviced" "{{ destdir / 'usr/sbin' }}"
    install -C "{{ rustdir / mode }}/squealog" "{{ destdir / 'usr/sbin' }}"
    tar -xvf {{ fetchdir }}/All/iwmbt-firmware* --include '*/share/iwmbt*' --strip-components 3 -C "{{ destdir / 'usr' }}"
    for pkg in {{ fetchdir }}/All/*kmod*.pkg; do \
        tar -xvf $pkg --include '*.ko' --strip-components 3 -C "{{ destdir / 'boot/kernel' }}"; \
    done

_tmpfs dir:
    mkdir -p "{{ dir }}"
    mount -t tmpfs tmpfs "{{ dir }}"

_installed_tmpdir: compile (_tmpfs tmpdir) (install tmpdir)

# Build an OS image on a given ZFS pool
image pool: _installed_tmpdir
    "{{ sudo }}" "{{ rustdir / mode }}/soft" -s "{{ pool / 'soft' }}" run --create "{{ img }}=/" --import "{{ tmpdir }}/=/" \
        --cmd "cd / && <METALOG sed -e 's|^./bin/|./usr/bin/|' -e 's|^./sbin/|./usr/sbin/|' -e '/^.\/root/d' -e '/timeout/d' -e '/etc\/ssl/d' | /usr/sbin/mtree -u || true"
    umount -f "{{ tmpdir }}"

_zpool mount name *devs:
    mkdir -p "{{ mount }}"
    "{{ sudo }}" zpool create -O canmount=off -O mountpoint=none -O atime=off -O compression=zstd-fast -m / -R "{{ mount }}" "{{ name }}" {{ devs }}
    "{{ sudo }}" zfs create -u -o soft:store=true -o readonly=on -o mountpoint=none "{{ name / 'soft' }}"
    "{{ sudo }}" zfs create -u -o setuid=off -o canmount=on -o mountpoint=/home "{{ name / 'home' }}"
    echo 'softbsdlive' | "{{ sudo }}" zfs create -u -o encryption=on -o keylocation=prompt -o keyformat=passphrase -o softbsd.user:uid=69696969 -o softbsd.user:name=live -o softbsd.user:groups=0 "{{ name / 'home/live' }}"

# Build a ZFS pool on a given device
pool dev: _installed_tmpdir (_zpool diskmount diskpool dev) (image diskpool)
    # TODO: loader not requiring bootfs! :)
    "{{ sudo }}" zpool set bootfs="{{ diskpool / 'soft' / img }}@S" "{{ diskpool }}"
    "{{ sudo }}" zpool export "{{ diskpool }}"

_ramdisk size unit:
    "{{ sudo }}" mdconfig -s "{{ size }}" -u "{{ unit }}"

# Build an EFI System Partition into a given file
efi_partition out:
    #!/bin/sh
    . base/tools/boot/install-boot.sh
    make_esp_file "{{ out }}" 2048 "{{ objdir / justfile_directory() / 'base' / machine + '.' + arch / 'stand/efi/loader_lua/loader_lua.efi' }}"
    # make_esp_file "{{ out }}" 2048 loader_lua.efi

# Build a full disk image into a given file (can be used directly with a ZEROED block device) using a ramdisk for the ZFS pool
disk out size="1g" md="69": _installed_tmpdir (efi_partition (buildtop / 'esp.img')) (_ramdisk size md) (pool ('md' + md))
    "{{ sudo }}" mkimg -v -s gpt -p efi:="{{ buildtop / 'esp.img' }}" -p freebsd-zfs:="/dev/md{{ md }}" -o "{{ out }}"
    # sh
    "{{ sudo }}" mdconfig -d -u "{{ md }}"

##### Miscellaneous stuff

# Configure git sparse-checkout to exclude base LLVM
rmllvm:
    git -C base sparse-checkout set --no-cone '/*' \
        '!/contrib/llvm-project/clang' '/contrib/llvm-project/clang/lib/Headers' \
        '!/contrib/llvm-project/llvm' '!/contrib/llvm-project/lldb' '!/contrib/llvm-project/lld'

# Try to authenticate against PAM (for PAM module development)
test_nss facility user pass:
    #!/usr/bin/env python3
    import pypamtest
    tc = pypamtest.TestCase(pypamtest.PAMTEST_AUTHENTICATE)
    print(pypamtest.run_pamtest("{{ facility }}", "{{ user }}", [tc], ["{{ pass }}"], []))

# Make an in-memory zpool for playing around with soft
test_soft size="1g" md="420":
    "{{ sudo }}" mdconfig -s "{{ size }}" -u "{{ md }}"
    "{{ sudo }}" zpool create -m /tmp/soft -O soft:store=true softtest "md{{ md }}"
